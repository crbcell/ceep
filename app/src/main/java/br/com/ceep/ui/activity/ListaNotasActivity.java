package br.com.ceep.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.ceep.dao.NotaDAO;
import br.com.ceep.databinding.ActivityListaNotasBinding;
import br.com.ceep.model.Nota;
import br.com.ceep.ui.activity.recyclerview.helper.callback.NotaTouchHelper;
import br.com.ceep.ui.recyclerview.ListaNotasAdapter;
import br.com.ceep.ui.recyclerview.adapter.listener.OnItenClickListener;

import static br.com.ceep.ui.activity.NotaAcivityConstantes.CHAVE_NOTA;
import static br.com.ceep.ui.activity.NotaAcivityConstantes.CHAVE_POSICAO;
import static br.com.ceep.ui.activity.NotaAcivityConstantes.CODIGO_REQUISICAO_ALTERA_NOTA;
import static br.com.ceep.ui.activity.NotaAcivityConstantes.CODIGO_REQUISICAO_INSERE_NOTA;
import static br.com.ceep.ui.activity.NotaAcivityConstantes.POSICAO_INVALIDA;

public class ListaNotasActivity extends AppCompatActivity {

    public ActivityListaNotasBinding binding;
    private ListaNotasAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadViewBinding();
        List<Nota> todasNotas = pegaTodasNotas();
        confuguraRecyclerView(todasNotas);
        configuraBotaoNovaNota();
    }


    private List<Nota> pegaTodasNotas() {
        NotaDAO dao = new NotaDAO();
        dao.insere(new Nota("GFI ", "Linux Fundo GFI"));
        dao.insere(new Nota("VarCotas ", "Importação e Exportacao"));
        dao.insere(new Nota("Groovy 87 ", "Executar Groovy"));
        dao.insere(new Nota("CapturaMercado ", "Capturar info de Mercado"));
        dao.insere(new Nota("GeraBds.exe ", "Grabar em BDRisco"));
        dao.insere(new Nota("Groovy 86 - 28 ", "Executar Groovy"));
        dao.insere(new Nota("Groovy MValue ", "Executar Groovy"));
        dao.insere(new Nota("Groovy P.Acoes ", "PEyyyymmdd.txt"));
        dao.insere(new Nota("Groovy P.Premio ", "Premio.txt"));
        dao.insere(new Nota("GeraBds.jar ", "Gravar na base"));
        dao.insere(new Nota("CurvasRW ", "Curvas RW"));
        return dao.todos();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    private void configuraBotaoNovaNota() {
        TextView botaoFormularioNotas = binding.listaNotasInsereNota;
        botaoFormularioNotas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vaiParaFormularioNovaNotaInsere();
            }
        });
    }

    private void vaiParaFormularioNovaNotaInsere() {
        Intent iniciaFormularioNota = new Intent(ListaNotasActivity.this, FormularioNotaActivity.class);
        startActivityForResult(iniciaFormularioNota, CODIGO_REQUISICAO_INSERE_NOTA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (ehResultadoInsereNota(requestCode, data)) {
            if (ehResultadoOk(resultCode)) {
                Nota notaRecebida = data.getParcelableExtra(CHAVE_NOTA);
                adiciona(notaRecebida);
            }
        }
        //nao precisa testar posicao pois ela possui valor padrao
        if (ehResultadoAlteraNota(requestCode, data)) {
            if (ehResultadoOk(resultCode)) {
                Nota notaRecebida = data.getParcelableExtra(CHAVE_NOTA);
                int posicaoRecebida = data.getIntExtra(CHAVE_POSICAO, POSICAO_INVALIDA);
                if (ehPosicaoValida(posicaoRecebida)) {
                    altera(notaRecebida, posicaoRecebida);
                } else {
                    Toast.makeText(this, "Erro em alterar a nota", Toast.LENGTH_SHORT);
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void altera(Nota nota, int posicao) {
        new NotaDAO().altera(posicao, nota);
        adapter.altera(posicao, nota);
    }

    private boolean ehPosicaoValida(int posicaoRecebida) {
        return posicaoRecebida > POSICAO_INVALIDA;
    }

    private boolean ehResultadoInsereNota(int requestCode, Intent data) {
        return ehCodigoRequisicaoInsereNota(requestCode) && temNota(data);
    }

    private boolean ehCodigoRequisicaoInsereNota(int requestCode) {
        return requestCode == CODIGO_REQUISICAO_INSERE_NOTA;
    }

    private boolean ehResultadoAlteraNota(int requestCode, Intent data) {
        return ehCodigoRequisicaoAlteraNota(requestCode) && temNota(data);
    }

    private boolean ehCodigoRequisicaoAlteraNota(int requestCode) {
        return requestCode == CODIGO_REQUISICAO_ALTERA_NOTA;
    }


    private boolean temNota(@Nullable Intent data) {
        //nem sempre o intent tem data. se entrar no formulario e voltar e ele retorna sem data e quebra a aplicacao. Por isso usar data != null
        return data != null && data.hasExtra(CHAVE_NOTA);
    }

    private boolean ehResultadoOk(int resultCode) {
        return resultCode == Activity.RESULT_OK;
    }

    private void adiciona(Nota nota) {
        new NotaDAO().insere(nota);
        adapter.adiciona(nota);
    }

    private void loadViewBinding() {
        binding = ActivityListaNotasBinding.inflate(getLayoutInflater());
        View viewBinding = binding.getRoot();
        setContentView(viewBinding);
    }


    private void confuguraRecyclerView(List<Nota> todasNotas) {
        RecyclerView listaNotas = binding.recycleView;
        configuraAdapter(todasNotas, listaNotas);
        configuraItemTouchHelper(listaNotas);
    }

    private void configuraItemTouchHelper(RecyclerView listaNotas) {
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(
                new NotaTouchHelper(ItemTouchHelper.UP | ItemTouchHelper.DOWN,
                        ItemTouchHelper.LEFT, adapter));
        itemTouchHelper.attachToRecyclerView(listaNotas);
    }


    private void configuraAdapter(List<Nota> todasNotas, RecyclerView listaNotas) {
        adapter = new ListaNotasAdapter(this, todasNotas);
        listaNotas.setAdapter(adapter);
        adapter.setOnItenClickListener(new OnItenClickListener() {
            @Override
            public void onItemClick(Nota nota, int posicao) {
                vaiParaFormularioNotaActivityAltera(nota, posicao);
            }
        });
    }

    private void vaiParaFormularioNotaActivityAltera(Nota nota, int posicao) {
        Intent abreFormularioComNota = new Intent(ListaNotasActivity.this,
                FormularioNotaActivity.class);
        abreFormularioComNota.putExtra(CHAVE_NOTA, nota);
        abreFormularioComNota.putExtra(CHAVE_POSICAO, posicao);
        startActivityForResult(abreFormularioComNota, CODIGO_REQUISICAO_ALTERA_NOTA);
    }

}
//Toast.makeText(ListaNotasActivity.this, nota.getTitulo(), Toast.LENGTH_SHORT).show();