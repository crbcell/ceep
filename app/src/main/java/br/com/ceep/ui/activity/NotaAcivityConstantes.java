package br.com.ceep.ui.activity;

public interface NotaAcivityConstantes {
    int CODIGO_REQUISICAO_INSERE_NOTA = 1;
    int CODIGO_REQUISICAO_ALTERA_NOTA = 2;
    int POSICAO_INVALIDA = -1;
    String CHAVE_POSICAO = "posicao";
    String CHAVE_NOTA = "nota";
}
