package br.com.ceep.ui.recyclerview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Collections;
import java.util.List;

import br.com.ceep.databinding.ItemNotaBinding;
import br.com.ceep.model.Nota;
import br.com.ceep.ui.recyclerview.adapter.listener.OnItenClickListener;

public class ListaNotasAdapter extends RecyclerView.Adapter<ListaNotasAdapter.ItemViewHolder> {

    private final Context context;
    private final List<Nota> notas;
    private OnItenClickListener onItenClickListener;

    public ListaNotasAdapter(Context context, List<Nota> notas) {
        this.context = context;
        this.notas = notas;
    }

    public void setOnItenClickListener(OnItenClickListener onItenClickListener) {
        this.onItenClickListener = onItenClickListener;
    }

    @NonNull
    @Override
    public ListaNotasAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        ItemNotaBinding viewEscolhida = ItemNotaBinding.inflate(layoutInflater, parent, false);
        return new ItemViewHolder(viewEscolhida);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        Nota nota = notas.get(position);
        holder.vincula(nota);
    }

    @Override
    public int getItemCount() {
        return notas.size();
    }

    public void altera(int posicao, Nota nota) {
        notas.set(posicao, nota);
        notifyDataSetChanged();
    }

    public void remove(int posicao) {
        notas.remove(posicao);
        notifyItemRemoved(posicao);
    }

    public void troca(int daPosicao, int paraAPosicao) {
        Collections.swap(notas, daPosicao, paraAPosicao);
        notifyItemMoved(daPosicao, paraAPosicao);
    }


    class ItemViewHolder extends RecyclerView.ViewHolder {
        private final TextView titulo;
        private final TextView descricao;
        private Nota nota;

        public ItemViewHolder(@NonNull ItemNotaBinding binding) {
            super(binding.getRoot());
            titulo = binding.itemNotaTitulo;
            descricao = binding.itemNotaDescricao;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItenClickListener.onItemClick(nota, getAdapterPosition());
                }
            });
        }

        public void vincula(Nota nota) {
            this.nota = nota;
            preencheCampos(nota);
        }

        private void preencheCampos(Nota nota) {
            titulo.setText(nota.getTitulo());
            descricao.setText(nota.getDescricao());
        }
    }

    public void adiciona(Nota nota) {
        //index 0 para incluir no topo da lista
        notas.add(0, nota);
        notifyDataSetChanged();
    }

}