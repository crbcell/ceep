package br.com.ceep.ui.activity.recyclerview.helper.callback;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import br.com.ceep.dao.NotaDAO;
import br.com.ceep.ui.recyclerview.ListaNotasAdapter;

public class NotaTouchHelper extends ItemTouchHelper.SimpleCallback {

    private final ListaNotasAdapter adapter;

    public NotaTouchHelper(int dragDirs, int swipeDirs, ListaNotasAdapter adapter) {
        super(dragDirs, swipeDirs);
        this.adapter = adapter;
    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
        final int daPosicao = viewHolder.getAdapterPosition();
        final int paraAPosicao = target.getAdapterPosition();
        trocaPosicaoDasNotas(daPosicao, paraAPosicao);
        return true;
    }

    private void trocaPosicaoDasNotas(int daPosicao, int paraAPosicao) {
        new NotaDAO().troca(daPosicao, paraAPosicao);
        adapter.troca(daPosicao, paraAPosicao);
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
        int posicaoDaNotaDeslisada = viewHolder.getAdapterPosition();
        removeNotaDeslisada(posicaoDaNotaDeslisada);
    }

    private void removeNotaDeslisada(int posicao) {
        new NotaDAO().remove(posicao);
        adapter.remove(posicao);
    }
}