package br.com.ceep.ui.recyclerview.adapter.listener;

import br.com.ceep.model.Nota;

public interface OnItenClickListener {
    void onItemClick(Nota nota, int posicao);
}
