package br.com.ceep.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import br.com.ceep.R;
import br.com.ceep.databinding.ActivityFormularioNotaBinding;
import br.com.ceep.model.Nota;

import static br.com.ceep.ui.activity.NotaAcivityConstantes.CHAVE_NOTA;
import static br.com.ceep.ui.activity.NotaAcivityConstantes.CHAVE_POSICAO;
import static br.com.ceep.ui.activity.NotaAcivityConstantes.POSICAO_INVALIDA;

public class FormularioNotaActivity extends AppCompatActivity {

    private ActivityFormularioNotaBinding binding;
    private int posicaoRecebida = POSICAO_INVALIDA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadViewBinding();
        setTitle("Insere Nota");
        Intent dadosRecebidos = getIntent();
        verificaSeTemHasExtra(dadosRecebidos);
    }

    private void verificaSeTemHasExtra(Intent dadosRecebidos) {
        //não é preciso testar posicao, pois ele tem valor default e não dará crash na aplicacao
        if(dadosRecebidos.hasExtra(CHAVE_NOTA) /*&& dadosRecebidos.hasExtra("posicao")*/){
            setTitle("Altera Nota");
            Nota notaRecebida = dadosRecebidos.getParcelableExtra(CHAVE_NOTA);
            posicaoRecebida = dadosRecebidos.getIntExtra(CHAVE_POSICAO, POSICAO_INVALIDA);
            preencheCampos(notaRecebida);
        }
    }

    private void preencheCampos(Nota notaRecebida) {
        TextView titulo = binding.formularioNotaTitulo;
        TextView descricao = binding.formularioNotaDescricao;
        titulo.setText(notaRecebida.getTitulo());
        descricao.setText(notaRecebida.getDescricao());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_formulario_nota_salva, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(ehMenuSalvaNota(item)){
            Nota notaCriada = criaNota();
            retornaNota(notaCriada);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean ehMenuSalvaNota(@NonNull MenuItem item) {
        return item.getItemId() == R.id.menuFormularioIcSalvaNota;
    }

    private void loadViewBinding() {
        binding = ActivityFormularioNotaBinding.inflate(getLayoutInflater());
        View viewBinding = binding.getRoot();
        setContentView(viewBinding);
    }

    private void retornaNota(Nota nota) {
        Intent resultadoIntencao = new Intent();
        resultadoIntencao.putExtra(CHAVE_NOTA, nota);
        resultadoIntencao.putExtra(CHAVE_POSICAO, posicaoRecebida);
        setResult(Activity.RESULT_OK,resultadoIntencao);
    }

    private Nota criaNota() {
        EditText titulo = binding.formularioNotaTitulo;
        EditText descricao = binding.formularioNotaDescricao;
        return new Nota(titulo.getText().toString(), descricao.getText().toString());
    }

}
